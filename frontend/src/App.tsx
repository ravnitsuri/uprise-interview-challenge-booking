import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Home from './pages/Home';
import BookACall from './pages/BookACall';
import ShowAllEvents from './pages/ShowAllEvents';
import { HomeStyles } from './components/HomeStyles';

function App() {
	return (
		<Router>
			<HomeStyles>
				<Switch>
					<Route path="/bookACall" component={BookACall} />
					<Route path="/showAllEvents" component={ShowAllEvents} />
					<Route path="/" component={Home} />
				</Switch>
			</HomeStyles>
		</Router>
	);
}

export default App;
