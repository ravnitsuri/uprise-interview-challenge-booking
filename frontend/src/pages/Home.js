import React from 'react';
import { Button } from '@uprise/button';
import { Link } from 'react-router-dom';
import { Card } from '@uprise/card';
import { H3 } from '@uprise/headings';
import { Row, Container } from '@uprise/grid';
import TopBar from '../components/TopBar';

function Home() {
	return (
		<React.Fragment>
			<TopBar title={"Home"} showLeft={false} />
			<Container>
				<Row>
					<Card shadow={true} className="white-card" backgroundColor="white" width="800px">
						<Row className="text-center">
							<Container>
								<H3>Select an action</H3>
							</Container>
						</Row>
						<Row className="button-center mt-4 pt-4">
							<Link to="/bookACall">
								<Button size="medium" fullWidth={false} title="Book A Call" />
							</Link>
						</Row>
						<Row className="button-center">
							<Link to="/showAllEvents">
								<Button size="medium" fullWidth={false} title="Show All Events" className="mt-3" />
							</Link>
						</Row>
					</Card>
				</Row>
			</Container>
		</React.Fragment>
	);
}

export default Home;
