import React from 'react';
import AvailableSlotsScreen from '../components/BookACall/AvailableSlotsScreen';
import BookingScreen from '../components/BookACall/BookingScreen';
import BookingSuccessful from '../components/BookACall/BookingSuccessful';
import moment from 'moment';

function BookACall() {
	const [ page, setPage ] = React.useState(null);

	const [ formData, setFormData ] = React.useState({
		date: moment().format('YYYY/MM/DD'),
		timezone: '',
		slotDuration: 15
	});

	const onBookingClick = (time) => {
		const addSlot = async () => {
			try {
				let res = await fetch(`${process.env.REACT_APP_BACKEND_BASEURL}/events/add`, {
					method: 'post',
					body: JSON.stringify({ ...formData, time }),
					headers: {
						'Content-Type': 'application/json'
					}
				});
				let jsonRes = await res.json();
				console.log({ jsonRes });
				setPage('BookingSuccessful');
			} catch (e) {
				console.log(e);
				alert('Error adding event.');
			}
		};
		addSlot();
	};

	switch (page) {
		case 'BookingScreen':
		default:
			return <BookingScreen setPage={setPage} formData={formData} setFormData={setFormData} />;

		case 'AvailableSlotsScreen':
			return <AvailableSlotsScreen setPage={setPage} formData={formData} onBookingClick={onBookingClick} />;

		case 'BookingSuccessful':
			return <BookingSuccessful setPage={setPage} />;
	}
}

export default BookACall;
