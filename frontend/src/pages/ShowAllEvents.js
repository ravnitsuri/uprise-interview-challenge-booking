import React from 'react';
import PickDateScreen from '../components/ShowAllEvents/PickDateScreen';
import ShowEventsScreen from '../components/ShowAllEvents/ShowEventsScreen';
import moment from 'moment';

function ShowAllEvents() {
	const [ page, setPage ] = React.useState('');
	const [ formData, setFormData ] = React.useState({ date: moment().format('YYYY/MM/DD') });

	const onShowEvents = () => {
		setPage('ShowEventsScreen');
	};

	switch (page) {
		case 'PickDateScreen':
		default:
			return (
				<PickDateScreen
					setPage={setPage}
					formData={formData}
					setFormData={setFormData}
					onShowEvents={onShowEvents}
				/>
			);

		case 'ShowEventsScreen':
			return <ShowEventsScreen setPage={setPage} formData={formData} />;
	}
}

export default ShowAllEvents;
