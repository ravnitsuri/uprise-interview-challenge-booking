import React from 'react';
import { Button } from '@uprise/button';
import Datepicker from '../Datepicker/Datepicker';
import { Row, Container } from '@uprise/grid';
import { Card } from '@uprise/card';
import { H5 } from '@uprise/headings';
import { Select } from '@uprise/form';
import TopBar from '../TopBar';
import { useHistory } from 'react-router-dom';
import { Loader } from '../Loader';
import moment from 'moment';

function BookingScreen({ setPage, formData, setFormData }) {
	const history = useHistory();
	const [ isLoading, setLoading ] = React.useState(true);
	const [ timezones, setTimezones ] = React.useState([]);

	React.useEffect(() => {
		const fetchData = async () => {
			let res = await fetch(`${process.env.REACT_APP_BACKEND_BASEURL}/events/timezones`);
			let jsonRes = await res.json();
			console.log({ jsonRes });
			setLoading(false);
			setTimezones(jsonRes);
			setFormData((formData) => ({ ...formData, timezone: jsonRes[0] }));
		};
		fetchData();
	}, [setFormData]);

	return (
		<React.Fragment>
			<TopBar title={'Book a Call'} backAction={() => history.push('/')} />
			<Container>
				<Row>
					<Card shadow={true} className="white-card" backgroundColor="white" width="800px">
						{isLoading ? (
							<Row>
								<Loader />
							</Row>
						) : (
							<React.Fragment>
								<Row>
									<div className="col-6">
										<H5>Select a Date</H5>
										<Datepicker
											date={moment(formData.date, 'YYYY/MM/DD')}
											onChange={(date) =>
												setFormData((formData) => ({
													...formData,
													date: date.format('YYYY/MM/DD')
												}))}
										/>
									</div>
									<div className="col-6">
										<Select
											className="mb-4"
											id="timezone"
											name="timezone"
											label={<H5>Select your timezone</H5>}
											options={timezones.map((x) => ({ value: x, label: x }))}
											value={formData.timezone}
											onChange={(e) =>
												setFormData((formData) => ({ ...formData, timezone: e.target.value }))}
										/>

										<Select
											id="timezone"
											name="timezone"
											label={<H5>Select call duration</H5>}
											options={[ 15, 30, 60 ].map((x) => ({ value: x, label: `${x} minutes` }))}
											value={formData.slotDuration}
											onChange={(e) =>
												setFormData((formData) => ({
													...formData,
													slotDuration: e.target.value
												}))}
										/>
									</div>
								</Row>
								<Button
									title="Get Free Slots"
									className="mt-2"
									onClick={() => {
										console.log({ formData });
										setPage('AvailableSlotsScreen');
									}}
								/>
							</React.Fragment>
						)}
					</Card>
				</Row>
			</Container>
		</React.Fragment>
	);
}

export default BookingScreen;
