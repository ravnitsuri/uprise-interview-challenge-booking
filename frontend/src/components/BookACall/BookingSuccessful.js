import React from 'react';
import { Bold } from '@uprise/text';
import SuccessIcon from '@uprise/icons/icons/png/success/success.png';
import { H3 } from '@uprise/headings';
import { Card } from '@uprise/card';
import { Row, Container } from '@uprise/grid';
import TopBar from '../TopBar';

function BookingSuccessful({ setPage }) {
	return (
		<React.Fragment>
			<TopBar title={'Booking a Coaching'} backAction={() => setPage('AvailableSlotsScreen')} />
			<Container>
				<Row>
					<Card shadow={true} className="white-card" backgroundColor="white" width="800px">
						<div className="row m-4">
							<div className="m-auto w-50 text-center">
								<img
									alt="Img1"
									className="d-block m-auto"
									src={SuccessIcon}
									width="90px"
									height="90px"
								/>

								<Bold>
									<H3 className="mt-5">Booking successful!</H3>
								</Bold>
							</div>
						</div>
					</Card>
				</Row>
			</Container>
		</React.Fragment>
	);
}

export default BookingSuccessful;
