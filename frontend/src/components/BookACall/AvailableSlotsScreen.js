import React from 'react';
import { Row, Container } from '@uprise/grid';
import { Card } from '@uprise/card';
import { H5 } from '@uprise/headings';
import { Button } from '@uprise/button';
import TopBar from '../TopBar';
import { Loader } from '../Loader';
import { serialize } from '../../util/url-serialize';

function AvailableSlotsScreen({ setPage, formData, onBookingClick }) {
	const [ isLoading, setLoading ] = React.useState(true);
	const [ timeSlots, setTimeSlots ] = React.useState([]);

	React.useEffect(() => {
		const fetchData = async () => {
			let queryString = serialize(formData);
			let res = await fetch(`${process.env.REACT_APP_BACKEND_BASEURL}/events/allAvailableSlots?${queryString}`);
			let jsonRes = await res.json();
			console.log({ jsonRes });
			setLoading(false);
			setTimeSlots(jsonRes);
		};
		fetchData();
	}, [formData]);

	return (
		<React.Fragment>
			<TopBar title={'Available Slots'} backAction={() => setPage('BookingScreen')} />
			<Container>
				<Row>
					<Card shadow={true} className="white-card" backgroundColor="white" width="800px">
						{isLoading ? (
							<Row>
								<Loader />
							</Row>
						) : (
							<React.Fragment>
								<H5 className="mb-4">Click on any of the time slots to book</H5>
								<Row>
									{timeSlots.map((x, i) => (
										<div className="col-sm-4 p-2" key={i}>
											<Button
												variant="tertiary"
												title={x}
												className=""
												onClick={(e) => {
													e.preventDefault();
													onBookingClick(x.split(' - ')[0]);
												}}
											/>
										</div>
									))}
								</Row>
							</React.Fragment>
						)}
					</Card>
				</Row>
			</Container>
		</React.Fragment>
	);
}

export default AvailableSlotsScreen;
