import Styled from 'styled-components';

const HomeStyles = Styled.div`
	.white-card {
		margin: 50px auto;
		padding: 50px;
	}

	.button-center {
		a {
			margin: auto;
		}
		button {
			padding: 20px
		}
	}

	.content-column {
		max-width: 43%;
    margin: auto;
    justify-content: center;
    display: flex;
    flex-direction: column;
    align-items: center;
	}

	.time-slots {
		background-color: #fff3e3;
		border-radius: 3px;
		font-size: 1vw;
		margin-bottom: 8px;
		max-width: 400px;
		margin-left: auto;
		margin-right: auto;
	}

	select.form-control {
		border: 0;
		border-radius: 0;
		border-bottom: 1px solid #ced4da;
	}

	.top-bar {
		position: relative;
		height: 99px;
		line-height: 99px;
		background: #fff;
		text-align: center;
		vertical-align: middle;
		box-shadow: 0px 4px 10px rgba(219, 221, 227, 0.095056);
		h1 {
			height: 99px;
			line-height: 99px;
		}

		.left-stuff {
			position: absolute;
			left: 0;
			padding: 0 20px;
			img {
				margin: 0 8px 0 10px;
				cursor: pointer;
			}
			span { 
				cursor: pointer;
			}
			color: #35359C;
			font-size: 20px;
			font-weight: 700;
			font-family: 'Proxima Nova Semibold';
			line-height: 100px;
		}
	}
`;

export { HomeStyles}