import React from 'react';
import DatePickerStyles from './DatepickerStyles';
import { DayPickerSingleDateController } from 'react-dates';
import 'react-dates/lib/css/_datepicker.css';
import 'react-dates/initialize';

function Datepicker({ date, onChange }) {
	return (
		<DatePickerStyles>
			<DayPickerSingleDateController
				onDateChange={onChange}
				date={date}
				numberOfMonths={1}
			/>
		</DatePickerStyles>
	);
}

export default Datepicker;
