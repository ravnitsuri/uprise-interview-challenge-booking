import React from 'react';
import { Button } from '@uprise/button';
import Datepicker from '../Datepicker/Datepicker';
import { Row, Container, Col } from '@uprise/grid';
import { Card } from '@uprise/card';
import { H3 } from '@uprise/headings';
import { useHistory } from 'react-router-dom';
import TopBar from '../TopBar';
import moment from 'moment';

function PickDateScreen({ setPage, formData, setFormData, onShowEvents }) {
	const history = useHistory();
	console.log({ formData });
	return (
		<React.Fragment>
			<TopBar title={'Show All Events'} backAction={() => history.push('/')} />
			<Container>
				<Row>
					<Card shadow={true} className="white-card" backgroundColor="white" width="800px">
						<Row>
							<Col className="content-column">
								<H3>Here are all the events on </H3>
								<Datepicker
									date={moment(formData.date, 'YYYY/MM/DD')}
									onChange={(date) =>
										setFormData((formData) => ({
											...formData,
											date: date.format('YYYY/MM/DD')
										}))}
								/>
								<Button
									title="Show events"
									onClick={(e) => {
										e.preventDefault();
										onShowEvents();
									}}
								/>
							</Col>
						</Row>
					</Card>
				</Row>
			</Container>
		</React.Fragment>
	);
}

export default PickDateScreen;
