import React from 'react';
import { Row, Container, Col } from '@uprise/grid';
import { Card } from '@uprise/card';
import { H3, H6 } from '@uprise/headings';
import TopBar from '../TopBar';
import moment from 'moment';
import { Loader } from '../Loader';
import { serialize } from '../../util/url-serialize';

function ShowEventsScreen({ setPage, formData }) {
	const [ isLoading, setLoading ] = React.useState(true);
	const [ events, setEvents ] = React.useState([]);

	React.useEffect(
		() => {
			const showAllBookedSlots = async () => {
				try {
					let sendObj = {
						startOfDate: moment(formData.date, 'YYYY/MM/DD').startOf('day').utc().unix(),
						endOfDate: moment(formData.date, 'YYYY/MM/DD').endOf('day').utc().unix()
					};
					let queryString = serialize(sendObj);
					let res = await fetch(`${process.env.REACT_APP_BACKEND_BASEURL}/events/get?${queryString}`);
					let jsonRes = await res.json();
					console.log({ jsonRes });
					setEvents(
						jsonRes.map(
							(x) =>
								`${moment.utc(x.startDateTime).local().format('h:mm A')} - ${moment
									.utc(x.endDateTime)
									.local()
									.format('h:mm A')} (local)`
						)
					);
					setLoading(false);
				} catch (e) {
					console.log(e);
					alert('Error fetching events.');
				}
			};
			showAllBookedSlots();
		},
		[ formData ]
	);

	return (
		<React.Fragment>
			<TopBar title={'Show All Events'} backAction={() => setPage('PickDateScreen')} />
			<Container>
				<Row>
					<Card shadow={true} className="white-card" backgroundColor="white" width="800px">
						{isLoading ? (
							<Row>
								<Loader />
							</Row>
						) : (
							<Row>
								<Col>
									<React.Fragment>
										<H3 className="mb-5 text-center">
											Here are all the events on {moment(formData.date).format('LL')}
										</H3>
										{events.map((x, i) => (
											<H6 key={i} className="time-slots px-3 py-1 text-center">
												{x}
											</H6>
										))}
									</React.Fragment>
								</Col>
							</Row>
						)}
					</Card>
				</Row>
			</Container>
		</React.Fragment>
	);
}

export default ShowEventsScreen;
