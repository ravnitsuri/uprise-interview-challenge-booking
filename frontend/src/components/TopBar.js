import React from 'react';
import logoHead from '@uprise/icons/icons/png/logo-head/logo-head.png';
import angleLeft from '@uprise/icons/icons/svg/angle-left.svg';
import { H1 } from '@uprise/headings';
import { useHistory } from 'react-router-dom';

function TopBar({ title = 'Home', showLeft = true, backAction }) {
  const history = useHistory();

	return (
		<nav className="top-bar">
			<div className="left-stuff">
				{showLeft && <img alt="Img1" className="angleLeft" src={angleLeft} onClick={backAction} />}
				<img alt="Img1" className="logoHead" src={logoHead} />
				<span onClick={() => history.push('/')}>uprise</span>
			</div>
			<H1>{title}</H1>
		</nav>
	);
}

export default TopBar;
