
const express = require('express');
const app = express();
const cors = require('cors')
const bodyParser = require('body-parser')

let PORT = process.env.PORT || 3000;

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(express.json());
app.use(cors())

// starting point 
app.use('/', require('./routes/index')); 

app.listen(PORT, () => {
    console.log(`Server connected to ${PORT} ....`)
})