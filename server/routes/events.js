const express = require('express');
const router = express.Router();
const admin = require('firebase-admin');
const serviceAccount = require('../config/firebase-admin-sdk.json');
const MomentRange = require('moment-range');
const timeEnv = require('../config/env.json');
const Moment = require('moment-timezone');
const moment = MomentRange.extendMoment(Moment);

// admin initialize
admin.initializeApp({
	credential: admin.credential.cert(serviceAccount),
	databaseURL: 'https://uprise-interview-backend.firebaseio.com'
});

// get all timezones
router.get('/timezones', async (req, res) => {
	const timezones = moment.tz.names();
	res.send(timezones);
});

router.get('/allAvailableSlots', async (req, res) => {
	try {
		let { date, timezone: outputTimezone, slotDuration: slotDuration } = req.query;
		console.log({ date, outputTimezone, slotDuration });

		if (!date || !outputTimezone || !slotDuration) {
			return res.status(400).send('Invalid inputs');
		}

		// logic
		let allAvailableSlots = [];

		let initialTimezone = timeEnv.timezone;
		let startTime = moment.tz(`${date} ${timeEnv.startHours}`, 'YYYY/MM/DD hh:mm A', initialTimezone);
		let endTime = moment.tz(`${date} ${timeEnv.endHours}`, 'YYYY/MM/DD hh:mm A', initialTimezone);

		let startTimeStamp = moment
			.tz(`${date} ${timeEnv.startHours}`, 'YYYY/MM/DD hh:mm A', initialTimezone)
			.utc()
			.unix();
		let endTimeStamp = moment.tz(`${date} ${timeEnv.endHours}`, 'YYYY/MM/DD hh:mm A', initialTimezone).utc().unix();

		let eventsRef = admin.firestore().collection('events');
		let eventsByDateObj = await eventsRef
			.where('startTimeStamp', '>=', startTimeStamp)
			.where('startTimeStamp', '<=', endTimeStamp)
			.get();
		const eventsByDate = eventsByDateObj.docs.map((doc) => ({ id: doc.id, ...doc.data() }));

		console.log({ eventsByDate });

		while (startTime < endTime) {
			let eventStart = startTime.clone();
			let eventEnd = startTime.clone().add(slotDuration, 'minutes');
			let eventRange = moment.range(eventStart, eventEnd);

			let overlaps = false;
			let overlappingEvent = null;
			for (let i = 0, l = eventsByDate.length; i < l; i++) {
				let dbEvent = eventsByDate[i];
				let dbEventRange = moment.range(moment.utc(dbEvent.startDateTime), moment.utc(dbEvent.endDateTime));

				if (eventRange.overlaps(dbEventRange)) {
					overlaps = true;
					overlappingEvent = dbEvent;
					break;
				}
			}

			if (!overlaps) {
				allAvailableSlots.push(
					`${startTime.tz(outputTimezone).format('LT')} - ${startTime
						.tz(outputTimezone)
						.add(slotDuration, 'minutes')
						.format('LT')}`
				);
				startTime.add(slotDuration, 'minutes');
			} else {
				startTime = moment.utc(overlappingEvent.endDateTime);
			}
		}
		return res.send(allAvailableSlots);
	} catch (error) {
		console.log(error);
	}
});

router.post('/add', async (req, res) => {
	try {
		let { date, timezone: outputTimezone, slotDuration, time } = req.body;
		console.log({ date, outputTimezone, slotDuration, time });

		// ==== sample data ==== 
		// let date = '2020/10/25';
		// let time = '10:30 AM';
		// let outputTimezone = 'Asia/Kolkata';
		// let slotDuration = '30';

		if (!date || !outputTimezone || !slotDuration || !time) {
			return res.status(400).send('Invalid inputs');
		}

		let storingObj = {
			startDateTime: moment.tz(`${date} ${time}`, `YYYY/MM/DD hh:mm A`, outputTimezone).utc().format(),
			startTimeStamp: moment.tz(`${date} ${time}`, `YYYY/MM/DD hh:mm A`, outputTimezone).utc().unix(),
			endDateTime: moment
				.tz(`${date} ${time}`, `YYYY/MM/DD hh:mm A`, outputTimezone)
				.add(slotDuration, 'minutes')
				.utc()
				.format(),
			endTimeStamp: moment
				.tz(`${date} ${time}`, `YYYY/MM/DD hh:mm A`, outputTimezone)
				.add(slotDuration, 'minutes')
				.utc()
				.unix(),
			duration: '60'
		};
		console.log({ storingObj });
		await admin.firestore().collection('events').add(storingObj);
		return res.send({ message: 'Event added' }).status(200);
	} catch (error) {
		console.log(error);
	}
});

router.get('/get', async (req, res) => {
	
	let { startOfDate, endOfDate } = req.query;

	// ==== sample data ==== 
	// let date = '2020/10/25';
	// let outputTimezone = 'Asia/Kolkata';
	// let startOfDate = moment.tz(date, `YYYY/MM/DD`, outputTimezone).startOf('day').utc().unix();
	// let endOfDate = moment.tz(date, `YYYY/MM/DD`, outputTimezone).endOf('day').utc().unix();

	if (!startOfDate || !endOfDate) {
		return res.status(400).send('Invalid inputs');
	}

	console.log({ startOfDate, endOfDate });
	let eventsRef = admin.firestore().collection('events');
	let eventsByDate = await eventsRef
		.where('startTimeStamp', '>=', Number(startOfDate))
		.where('startTimeStamp', '<=', Number(endOfDate))
		.get();
	const data = eventsByDate.docs.map((doc) => ({ id: doc.id, ...doc.data() }));

	return res.send(data);
});

module.exports = router;
