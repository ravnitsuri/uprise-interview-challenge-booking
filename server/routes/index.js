const express = require("express");
const router = express.Router();

// root for events
router.use("/events", require("./events"));

module.exports = router; 